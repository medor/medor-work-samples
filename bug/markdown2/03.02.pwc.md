Contrôle bâclé ou conflit&nbsp;d’intérêts ?
=====================================

En 2014, la filiale belge de PwC (PricewaterhouseCoopers), réseau américain d’entreprises spécialisées dans l’audit informatique, contrôle la qualité et la conformité des logiciels utilisés par les sociétés privées en charge du vote électronique. Parmi elles, Stésud, dont le système Jites a buggé. S’il ne fait aucun doute que celui-ci présentait des failles, n’était-ce pas le rôle de l’auditeur de tirer la sonnette d’alarme ?

Fin janvier 2015, la RTBF l’annonce : « On connaît, enfin, tout le processus qui a conduit au gigantesque bug du vote électronique du 25 mai dernier : c’est une bonne vieille histoire de code source informatique. C’est la société Stésud qui a rédigé le code du logiciel responsable du comptage des voix. Mais c’est PricewaterhouseCoopers (PwC) qui devait contrôler ce code. Et le bug lui a échappé. » 

« Tout ira bien »
-----------------

Et voilà, c’est très clair : Stésud utilise un code défaillant mais PwC est responsable de ne pas s’en être rendu compte. Comme le souligne Stéphan De&nbsp;Mul, porte-parole du Service Public Fédéral (SPF) Intérieur, le bug est « passé au travers des mailles de tous les tests ». Le&nbsp;4 avril, à un mois du scrutin, PwC avait remis son audit sur le système de Stésud et se disait en mesure de « conclure avec une assurance raisonnable, mais pas absolue », que ça allait fonctionner sans problème… 
Comment ont-ils réalisé des tests pronostiquant l’inverse de ce qui s’est finalement passé ?  Détail embêtant : personne ne connaît les tests en question. Comment sur une batterie moyenne de milliers d’examens, une erreur de retour de case aussi grossière ne peut-elle pas être identifiée ? Impossible d’obtenir la réponse. Tenue par une clause de confidentialité, PwC ne peut pas s’exprimer. En bref, ceux qui savent sont tenus au secret et ceux qui ignorent peuvent parler…

Nous avons interrogé notre hacker anonyme : « Bien sûr, Stésud est responsable mais PwC est également responsable de laisser passer un code d’aussi mauvaise qualité. » Et, pourtant, des tests efficaces existent et sont utilisés, constamment, dans d’autres domaines: « Les cartes bancaires, cartes d’identité, passeports, terminaux de payement, etc. doivent se soumettre à des contrôles très stricts réalisés par des laboratoires sous le contrôle des États. Il suffirait de soumettre les éléments logiciels et hardwares du vote électronique au même contrôle qui est, croyez-moi, bien plus rigoureux que ce que PwC a pu entreprendre. » 
Le code utilisé par Stésud est-il à ce point dépassé qu’il est difficile de l’auditer ? PwC aurait-il effectué son audit à la légère ? S’il est impossible d’obtenir les informations techniques permettant de trancher, nous pouvons au minimum épingler un élément étonnant concernant les liens entre Stésud et PwC, l’audité et l’auditeur.


Le contrôlé payé par&nbsp;le&nbsp;contrôleur
----------------------------------

Le SPF Intérieur a agréé cinq sociétés qui peuvent contrôler la qualité et la conformité des logiciels utilisés pour le vote électronique – histoire d’avoir un garde-fou. Ensuite, c’est à la société en charge de ce logiciel d’engager elle-même son contrôleur… C’est donc Stésud qui rémunère PwC. Le but du jeu, selon Stéphan De Mul (porte-parole du SPF Intérieur) est de mettre le SPF Intérieur hors du processus de contrôle pour assurer l’indépendance de cet audit vis-à-vis des pouvoirs publics.
Le garde-fou PwC semble fragile pour deux raisons :

- Non seulement, le contrôleur est payé par le contrôlé. Mais de surcroît, en 2014, PwC était commissaire au comptes de Stésud. Ces&nbsp;différentes casquettes (commissaire, auditeur) peuvent-elles court-circuiter l’indépendance et la rigueur d’un jugement ? Notons qu’il y a quand même un peu d’argent à la clé. En 2005, le&nbsp;contrôle des logiciels pour le vote électronique par un organisme agréé était chiffré à 520 000 euros, d’après le ministère de l’Intérieur.
- Les logiciels testés en 2014 étaient en fin de vie. Le contrôle a peut-être été un peu superficiel. À&nbsp;en croire un des membres du Collège des experts qui a accepté de nous parler, déjà en 2012, l’audit était léger. Selon lui, PwC n’aurait même pas relu le code. Les mises à jour et audits du logiciel auraient-ils été moins poussés parce que l’outil n’allait plus être utilisé après 2014 ? 

<div markdown="1" class="directions">
Tu veux entendre la version de PwC sur le bug ? Appelle le +32&nbsp;(0)483 68 62 76.
</div>

<div markdown="1" class="directions right">
Sinon, poursuis ton chemin et découvre le 3<sup>e</sup> suspect: [l’État](#etat-belge).
</div>
