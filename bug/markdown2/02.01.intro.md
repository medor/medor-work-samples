<div markdown="1" class="directions">
En intro de cette enquête dont tu es le héros, voici les lieux, les suspects et l’arme du crime. Ensuite, balade-toi dans l’enquête au gré de tes envies, selon les choix multiples qui te sont proposés. Prends garde à ne pas faire bugger ton enquête.
</div>

Souviens-toi. 

Nous sommes en Belgique, le 25 mai 2014. C’est la « mère » de toutes les élections, qui rassemble les fédérales, européennes, régionales et communautaires. Tous les yeux sont braqués sur les nationalistes flamands de la N‑VA, dont le succès risque de faire chanceler l’État belge.

À quelques jours du vote, Étienne Van Verdegem, chef de la cellule « Élections » du ministère de l’Intérieur, rassure le Parlement fédéral : « Je suis persuadé que le 25 mai 2014, le vote électronique se déroulera sans problèmes. » Ouf.

Sauf que, rappelle-toi... Le bug informatique, le cafouillage monstre, les 2 250 voix perdues, la remise en cause immédiate du vote électronique. Pendant dix jours, c’est le branle-bas de combat. Le ministère de l’Intérieur regarde les sociétés privées rechercher la solution. 

À première vue, le pourcentage de voix concernées est infime&nbsp;: 0,03 % de l’ensemble des électeurs inscrits dans le royaume. Mais rapporté aux électeurs votant par carte magnétique à Bruxelles et en Wallonie (un peu plus d’un million), ce pourcentage grimpe et concerne 0,21 % de ces citoyens, rappelle David Glaude, de l’association Pour une Éthique du Vote Automatisé (PourEVA). Et même si la répartition des sièges entre partis n’a pas été affectée, l’épisode « bug 2014 » questionne notre système de vote. 
Est-il encore aux mains des pouvoirs publics ? À qui confie-t-on nos élections ? Et à quel prix ? Y-a-t-il un coupable ?

Le professeur Olivier Pereira du département de cryptographie de l’Université catholique de Louvain (UCL), spécialiste du vote électronique, pose une bonne question : « Je voudrais trouver un autre milieu dans le domaine de l’informatique où on utilise un système mis en place il&nbsp;y a presque 25 ans et dont on espère encore qu’il fonctionne. »

Car oui, lecteur, la démocratie belge (et toi avec) est entrée dans le XXI<sup>e</sup> siècle avec… un outil du Moyen-Âge informatique : la disquette. 

S’il a subi des petites adaptations au fil des années, le système utilisé date de 1994, soit l’année de la mort d’Ayrton Senna, de la « chanson » _The Rhythm of the Night_ ou encore de l’inauguration du tunnel sous la Manche.

L’État belge serait-il un fan de vintage qui s’ignore ? Non, il a confié la programmation et la surveillance des systèmes informatiques de vote à des firmes privées, perdant assez vite le contrôle de la situation.
