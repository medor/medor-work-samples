<div class="entry">VALIDATION BRUXELLES</div>

Sans titre
==========

Le 10 juin 2014, le rapport du Collège des experts est déposé au Parlement
bruxellois qui est pressé de se composer. Ce rapport rappelle entre autre que
« le SPF Intérieur était complètement dépendant de la société Stésud », que ça
a pataugé jusqu’au 5 juin. Côté sécurité, « les mécanismes mis en place dans
le processus électoral ne sont pas, en théorie, suffisants pour garantir la
sécurité dudit processus ».

Du lourd.

La réaction des nouveaux élus suite à ce rapport ne s’est pas fait attendre.
Cinglante, nette et sans bavure. Pour valider les élections, la Belgique a mis
au point un système imparable. Ce sont les nouveaux élus qui valident leur
propre élection. On n’est jamais trop prudent.

Pour y parvenir, sept membres de la « commission de validation des opérations
électorales » sont tirés au sort. En 2014, cela a donné 4 PS, 1 cdH, 1 Ecolo
et 1 VB.

MM. Fassi (cdH) est président et Azzouzi (PS) rapporteur de la commission très
spéciale de validation expresse des élections bruxelloises.

<img src="./img/luci/bug.jpg">

<!---
Ce 10 juin, la plénière est suspendue à 14h48 afin qu’ils puissent lire les 50
pages du rapport, débattre, et prendre position. 1h17 plus tard, la séance
reprend. En retirant le déplacement vers un salon, une pause pipi, voire un
café, un coup de téléphone, cela une heure de lecture et discussions. Selon un
témoin, les élus ont mis 15 minutes pour lire le rapport. C’est un fait
exceptionnel qui n’a pas été relaté par les médias. Le rapport du Collège fait
environ 30.000 mots. Un adulte moyen lit 300 mots à la minute. Mais hasard
merveilleux, les 7 élus politiques tirés au sort ne sont PAS des ‘adultes
moyens’, ce sont des lecteurs plus rapides que les lecteurs rapides (1500
mots/minute). Puisqu’en lisant le dossier en 15 minutes, ils ont été flashés à
2000 mots la minute. Une autre explication que nous n’oserions avancer : ils
ont bâclé la lecture du rapport.

Au final, la commission a validé les résultats, 5 voix pour et deux voix
contre. Autant dire que le politique s’assied sur le dossier jusqu’aux
prochaines élections… Vous avez dit « formalité » ?
-->
