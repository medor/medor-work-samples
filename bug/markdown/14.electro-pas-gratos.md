<div class="entry">ELECTRO PAS GRATO</div>

Sans titre
==========

Quand le vote électronique est lancé, le contrat de décembre 1992 de Stésud
avance comme coût : 210.583.844 BEF (soit 5.220.237€) pour le matériel et
l’installation. L’assistance technique lors de l’organisation de chaque
scrutin s’élève alors à 4.119.149 BEF (soit 102.111 €).

Quelques élections plus tard, le Sénat a publié en 2005 une comparaison entre
les couts du vote automatisé et du vote papier. Résultat : le vote
électronique coûte trois fois plus cher que le papier.

Côté ‘nouveau système’ (Smartmatic), Charles Picqué (Ministre-Président de la
Région bruxelloise) déclarait fin 2011 que le coût estimé serait « d'environ
deux euros par vote », soit un coût toujours supérieur au vote papier.

Depuis 2012, Fédéral, Flandre et Bruxelles ont opté pour le système
Smartmatic. Le coût total du développement du système et de l’achat des
ordinateurs a coûté 29,4 millions d’euros en 2012, dont 22,4 pour la Flandre,
6,42 pour le Fédéral et 0,5 millions pour Bru xelles.

L’adaptation des logiciels pour les élections suivantes seront financée sur
une base de tarif horaire.

Au total et sur base de l’appel d’offre, la valeur du contrat oscille entre
150 et 250 millions d'euros.

Smartmatic ? C’est quoi ce nouveau système ?

<div markdown="1" class="directions">
* %%Va à la page ET DEMAIN (et ne discute pas, il n’y a qu’un choix)%%
</div>
