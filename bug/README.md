Comment générer le fichier HTML contenant les articles?
=======================================================

Il vous faut tout d'abord installer les dépendances nécessaires, le plus simple
est d'utiliser le gestionnaire de paquets de python

    virtualenv --no-site-packages venv
    source venv/bin/activate
    pip install -r requirements.txt

Ensuite il suffit de taper

    make

Cette commande compile les fichiers markdown du dossier éponyme vers le fichier
`stories/bug.html`.

Puis
    python -m SimpleHTTPServer

et visiter dans un navigateur compatible (Arora ou Epiphany actuellement)
    http://localhost:8000/

Chaque fois que vous modifiez l'un des fichiers markdown, retapez la commande
`make` pour générer une version à jour du fichier `stories/bug.html`.

Si problème, essayer un
     make clean && make
qui force à regénerer tous les fichiers
